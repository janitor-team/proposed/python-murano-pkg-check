# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.

pbr>=1.8 # Apache-2.0
PyYAML>=3.10.0 # MIT
yaql>=1.1.0 # Apache 2.0 License
six>=1.9.0 # MIT
stevedore>=1.17.1 # Apache-2.0
semantic-version>=2.3.1 # BSD
oslo.i18n>=2.1.0 # Apache-2.0
